#include <iostream>

#include "Person.h"
#include "Employee.h"

using namespace std;

int main () {
//    Employee emp { "David", 23, "Junior Developer" };
//
//    Person& person = emp;
//
//    Employee * empPointer = reinterpret_cast<Employee*>(&person);
//
//    cout << empPointer->position << endl;
//
//    person.printDetails();
//
//    const Person person2 = emp;
//
//    person2.printDetails();

//    Employee * empPointer2 = dynamic_cast<Employee*>(&person2);
//    cout << empPointer2->position << endl;

    Person * emp3 = new Employee{"John", 32, "Student"};

    delete emp3;

    return 0;
}