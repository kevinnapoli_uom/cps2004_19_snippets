#pragma once

#include <string>

class Person {
public:
    // To delete Employee from a Person reference, a virtual destructor is a must!
    virtual ~Person();
    Person(const std::string& name, int age);

    std::string name;
    int age;

    //void printDetails
    virtual void printDetails() const;
};