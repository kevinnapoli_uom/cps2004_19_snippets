#include "Person.h"

class Employee : public Person
{
public:
    Employee(const std::string& name, int age, const std::string& position);
    ~Employee();

    void printDetails() const override;
    std::string position;
};


