#include "Person.h"

#include <iostream>

using namespace std;

void Person::printDetails() const {
    cout << "Name: " << name << " age: " << age << endl;
}

Person::Person(const std::string& name, int age)
    : name(name), age(age)
{}

Person::~Person() {
    cout << "Person Destroyed" << endl;
}
