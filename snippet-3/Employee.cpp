#include "Employee.h"

#include <iostream>

using namespace std;

Employee::Employee(const std::string &name, int age, const std::string &position)
    : Person(name, age), position(position)
{

}

void Employee::printDetails() const {
    cout << "Position: " << position << endl;
}

Employee::~Employee() {
    cout << "Employee Destroyed" << endl;
}
