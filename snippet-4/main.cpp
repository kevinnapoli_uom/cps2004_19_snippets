#include <iostream>
#include "MyObject.h"

#include <memory>
#include <array>

using namespace std;

// Passing native array
//void foo(int n[]) {
//    n[1] = 1;
//}

// This template changes the second element
// of the passed array-like type
template<typename T>
void foo(T &n) {
    n[1] = 1;
}

// Computes the factorial of N at compile-time
template<int N>
struct Factorial{
    enum {
        Value = N * Factorial<N - 1>::Value
    };
};

// Base case
template<>
struct Factorial<0> {
    enum {
        Value = 1
    };
};

int main () {

    int nums[3] = {};
    cout << nums[1] << endl;
    foo(nums); // calls foo template function for type int[3]
    cout << nums[1] << endl;

    array<int, 6> myArray = {};
    cout << myArray[1] << endl;
    foo(myArray); // calls foo template function for type std::array<int, 6>
    cout << myArray[1] << endl;

    // Computes the factorial of 5 at compile-time
    constexpr int myValue = Factorial<5>::Value;
    cout << myValue << endl;

    // Static assert is evaluated during compilation..
    // If it fails, then compilation fails.
    static_assert(myValue == 120);

    // Shared pointers can refer to the same object
    // Unique pointers cannot refer to the same object (except via weak_ptr, etc)
    // Unique pointers own the underlying object
    shared_ptr<MyObject> smartObject = make_shared<MyObject>();
    {

        shared_ptr<MyObject> smartObject2 = smartObject;

        // Check if smart pointer points to something
        if (smartObject) {
            cout << "smartObject owns a pointer to MyObject" << endl;
        }

        if (smartObject2) {
            cout << "smartObject2 owns a pointer to MyObject" << endl;
        }
    }

    cout << "Something" << endl;
    //cout << (smartObject.get() == smartObject2.get()) << endl;

    return 0;
}