#include "MyObject.h"

#include <iostream>

using namespace std;

MyObject::MyObject() {
    cout << "Object Constructor" << endl;
}

MyObject::~MyObject() {
    cout << "Object Destructor" << endl;
}