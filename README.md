# CPS2004 Tutorial Snippets

A collection of C++ snippets for code demonstrated during the tutorials.

# Build and Run

Simply open the root folder in an IDE that supports CMake (such as CLion) or build manually in console.

To build manually, follow the steps below inside the repository directory.

```bash
mkdir build
cd build
cmake ..
make
```

At this point, the executables will be found in the build directory. For example, the snippet one executable is found under build/snippet-1. To run the executable (while in the build directory) use:

```bash
cd snippet-1
./snippet-1
```

or simply

```bash
./snippet-1/snippet-1
```
