#include <iostream>
#include <cstring>
#include <string>
#include <stdexcept>

using namespace std;

template<typename T>
class A {
public:
    A() : count()
    {
        items = new T[0];
    }

    ~A() {
        delete[] items;
    }

    void add(const T& item) {
        PointerType newBuffer = new T[count + 1];
        memcpy(newBuffer, items, sizeof(T) * count);
        newBuffer[count] = item;
        delete[] items;
        items = newBuffer;
        ++count;
    }

    std::size_t getCount() const {
        return count;
    }

    T& getItem(std::size_t index) const {
        if (index > count - 1) {
            throw runtime_error("Out of range");
        }

        return items[index];
    }

    using MyType = T;

private:
    using PointerType = T*;
    T * items;
    std::size_t count;
};

// Emulating static inheritance
template<typename D>
struct AbstractType {

    void callDerivedMethod() {
        // this is of type AbstractType<D>
        static_cast<D*>(this)->foo();
    }

};

struct ConcreteType : public AbstractType<ConcreteType> {
    void foo() {
        cout << "Concrete Type" << endl;
    }
};

// This class can be used at compile time
struct Square {
    constexpr Square(int x) : x(x) {}
    constexpr int getArea() const { return x*x; }
    int x;
};

// A 3D Vector
struct Vector {
    Vector() : x(), y(), z() {}
    explicit Vector(float xyz) : x(xyz), y(xyz), z(xyz) {}
    Vector(float x, float y, float z) : x(x), y(y), z(z) {}

    void print() {
        cout << "Vector: " << x << ", " << y << ", " << z << endl;
    }

    Vector operator+(const Vector& other) {
        return Vector(x + other.x, y + other.y, z + other.z);
    }

    float operator[](std::size_t index) {
        return xyz[index];
    }

    union {
        float xyz[3];
        struct {
            float x, y, z;
        };
    };
};

int main () {
    Vector vec(3.f);
    Vector vec2(1.f, 2.f, 3.f);
    auto result = vec2 + Vector(10.f);
    result.print();

    cout << result[1] << endl;

    Square square(3);
    cout << square.getArea() << endl;
    //static_assert(square.getArea() == 10);

    ConcreteType concreteType;
    AbstractType<ConcreteType> & ref = concreteType;
    ref.callDerivedMethod();

    cout << "Snippet-5" << endl;

    using MyList = A<string>;
    MyList a;
    a.add("helo");
    a.add("test");

    for (std::size_t i = 0; i < a.getCount(); ++i) {
        cout << a.getItem(i) << endl;
    }

    decltype(a)::MyType s = a.getItem(1);
    A<int>::MyType c;
    c = 2;
    cout << c << endl;

    return 0;
}