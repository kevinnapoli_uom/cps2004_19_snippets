#pragma once


class Foo {
public:
    // Constructor
    Foo();

    // Copy constructor
    Foo(const Foo& otherFoo);

    // Destructor
    ~Foo();

    // Assignment operator
    Foo& operator=(const Foo& otherFoo);
};

