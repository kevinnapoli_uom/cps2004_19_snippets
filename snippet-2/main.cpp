#include <iostream>

#include <string>

#include "Foo.h"

using namespace std;

void myMethod(const string& myString) {
    cout << myString << endl;
}

// Passing by const-reference allows one to pass
// temporary objects (rvalues).
// Note that passing temporary objects (rvalues) by value in C++17
// does not incur an additional copy due to mandatory copy elision
// It also allows one
// to pass lvalue objects without incurring additional copies.
void fooMethod(const Foo& foo) {

}

int main() {

    int b = 2;

    // Pointer to variable 'b'
    int * p = &b;

    // Reference to variable 'b'
    int& refToB = b;

    // modify 'b' through pointer
    *p = 3;

    // Disassociate pointer 'p' from variable 'b'
    p = nullptr;

    // modify 'b' through reference
    refToB = 4;

    //cout << "Hello world: " << b << endl;

    string str = "OOP";
    //myMethod("hello");

    // Constructor
    Foo firstFoo;

    // Constructor
    Foo secondFoo;

    // Assignment operator invoked
    secondFoo = firstFoo;

    // Copy Constructor invoked
    Foo thirdFoo = firstFoo;

    // Chaining of assignment operator
    thirdFoo = secondFoo = firstFoo;

    // Passing an rvalue (temporary object) to fooMethod
    fooMethod(Foo());
    return 0;
}