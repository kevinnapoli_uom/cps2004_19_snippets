#include "Foo.h"

#include <iostream>

using namespace std;

Foo::Foo() {
    cout << "Foo Constructor" << endl;
}

Foo::Foo(const Foo &otherFoo) {
    cout << "Foo Copy Constructor" << endl;
}

Foo::~Foo() {
    cout << "Foo Destructor" << endl;
}

Foo& Foo::operator=(const Foo &otherFoo) {
    cout << "Foo Operator =" << endl;
    return *this;
}