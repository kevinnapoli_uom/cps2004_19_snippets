#pragma once

// Shape Interface
class Shape {
public:
    // Pure virtual method - makes this class abstract/interface
    // and hence it cannot be initialised by calling new Shape();
    // Only sublclasses can get initialised (if not abstract)
    virtual int getArea() const = 0;
};

