//
// Created by kevin on 11/4/19.
//

#pragma once

#include "Shape.h"

class Square : public Shape
{
public:
    Square(int sideLength);

    // While the override keyword is optional, always use it if your
    // intention is to override. Otherwise, if in the base class the
    // virtual keyword is forgotten, a new method is created here instead
    // of erroring out. Can lead to lengthy debugging sessions!
    int getArea() const override;
private:
    int sideLength;
};

