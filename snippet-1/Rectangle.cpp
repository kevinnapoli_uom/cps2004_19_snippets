#include "Rectangle.h"

#include <iostream>

// note: try to always initialise primitive data types
Rectangle::Rectangle(int a, int b)
    : sideA(a), sideB(b)
{
}

Rectangle::~Rectangle() {
    std::cout << "Rectangle Destructor" << std::endl;
}

int Rectangle::getArea() const {
    return sideA * sideB;
}