//
// Created by kevin on 11/4/19.
//

#include "Square.h"

Square::Square(int sideLength)
    : sideLength(sideLength)
{
}

int Square::getArea() const {
    return sideLength * sideLength;
}