#pragma once

#include "Shape.h"

class Rectangle : public Shape
{
public:
    Rectangle(int a, int b);

    virtual ~Rectangle();

    int getArea() const override;
private:
    int sideA, sideB;
};


