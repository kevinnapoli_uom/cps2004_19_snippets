#include <iostream>

#include <vector>

#include "Square.h"
#include "Rectangle.h"

using namespace std;

// The pragma pack code, if uncommented, forces the Class A to pack its members tightly
//#pragma pack(1)
class A {
    // If any of the methods in a class are virtual,
    // the sizeof(A) increases. This is implementation dependant,
    // but usually a hidden pointer to the virtual table is implicitly
    // injected in the class (hidden data member) having sizeof(void*).
    virtual void doSomething() = 0;
    int x, y, z;
};

int main() {

    Square square(5);

    // lookup RAII idiom
    {
        Rectangle rectangle(3, 5);

        Shape *myShape = &rectangle;

        cout << myShape->getArea() << endl;

        // Rectangle destructor is called here as rectangle goes out of scope
    }

    cout << square.getArea() << endl;

    cout << sizeof(A) << endl;

    Rectangle * ptRectangle = new Rectangle(10, 9);

    cout << ptRectangle->getArea() << endl;

    // Delete manually, heap allocated objects need to be managed by the developer
    delete ptRectangle;

    // Don't delete twice, it is a good idea to set ptRectangle = nullptr; after deleting
    //delete ptRectangle;

    return 0;
}