// Type your code here, or load an example.

#include <iostream>

using namespace std;

template <typename T>
void printMe(T x) {
    auto typeName = typeid(T).name();
    T t = 2;
    cout << typeName << ": "  << x << endl;
}

template<>
void printMe<const char*>(const char* x) {
    cout << "Special" << ": "  << x << endl;
}

template<typename ... T>
void printMultiple(T ... t) {
    (std::cout << ... << t) << endl;
}

template <int N>
struct Fibonacci {
    static constexpr int value = Fibonacci<N-2>::value + Fibonacci<N-1>::value;
};

template<>
struct Fibonacci<1> {
    static constexpr int value = 1;
};

template<>
struct Fibonacci<0> {
    static constexpr int value = 0;
};

int main () {
    printMe("hello");
    printMe(2);

    printMultiple(123, "abc");
    return 0;
}